FROM node:12 AS build
WORKDIR /source

COPY . .
RUN npm install
RUN npm run build
RUN cp -r node_modules /source/dist/node_modules
COPY pipeline_resources/deploy/run.sh /source/dist/run.sh
RUN chmod +x /source/dist/run.sh

# Run the application
FROM node:12
WORKDIR /app
EXPOSE 8000
COPY --from=build /source/dist .
ENTRYPOINT ["node", "/app/index.js"]