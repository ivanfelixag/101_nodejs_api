
(async () => {  
  try {
    const { App } = require('./App');
    new App().run();
  } catch (error) {
    console.log(error);
  }
})();