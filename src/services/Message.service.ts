import { IMessage } from "../interfaces/Message.interface";
import config from "../config";

class MessageService {

  public async retriveMessage(): Promise<IMessage> {
      const message = {
        message: new String(config.get("MESSAGE"))
    } as IMessage;
    return message;
  }

}

export default new MessageService();