import express from 'express';

/**
 * Base server instance without listent to any port.
 * @const
 */
const app = express();

export default app;
