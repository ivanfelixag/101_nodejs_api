import MessageService from "../services/Message.service"
import config from "../config";

test('should return environment message from message service', async () => {
    const result = await MessageService.retriveMessage();
    const givenMessage: string = result.message.toString();
    const expectedMessage: string = config.get("MESSAGE");
    expect(givenMessage == expectedMessage).toBe(true)
})