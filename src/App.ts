import app from "./server";
import { Application } from "express";
import ApplicationRoutes from "./routes";
import config from "./config";

export class App {
  private express: Application;

  constructor() {
    this.express = app;
    this.middleware();
    this.routes();
    this.cron();
  }

  public run() {
    console.log(config.get("VCAP_APP_PORT"));

    this.express.listen(config.get("VCAP_APP_PORT"), () => {
        console.log(`App listening on port ${config.get("VCAP_APP_PORT")}`);
    });
  }

  private middleware(): void {
  }

  private routes(): void {
    this.express.use(
      `${config.get("BALANCER_CONTEXT")}`,
      new ApplicationRoutes().getRoutes()
    );
  }

  private cron(): void {
    // let job = new CronJob("00 00 06 * * *", () => {
    //   console.log("Cron started");
    //   Job.Init();
    // });

    // job.start();
  }
}
