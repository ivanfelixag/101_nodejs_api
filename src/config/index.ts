import env from '../utils/env'
class Config {

  public _config: any = {};

  constructor() {
    this.set("VCAP_APP_PORT", env('VCAP_APP_PORT', '8000'));
    this.set("BALANCER_CONTEXT", env('BALANCER_CONTEXT', '/prueba1'));
    this.set("MESSAGE", env('MESSAGE', 'Hello Jesus, Azahara y Manu - Prueba 1 - Con Service discovery'));
  }

  public get(key: string): any {
    return this._config[key];
  }

  private set(key: string, value: any): void {
    this._config[key] = value;
    Object.defineProperty(this, key, {
      get: () => { return this._config[key] },
      set: (value: any) => { }
    })
  }
}

export default new Config();
