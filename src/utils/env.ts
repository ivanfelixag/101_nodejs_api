const TRUE_VALUES = [ 'true', 'TRUE', 'True', '1', 1, true ];
const FALSE_VALUES = [ 'false', 'FALSE', 'False', '0', 0, false ];

function isNumber (val: any) {
  return !isNaN(parseFloat(val)) && isFinite(val);
}


/**
 * Return an env variable or the default value if specified.
 *
 * @param {string} envvar The env variable you want
 * @param {(object|string|array|number|boolean)} [defaultValue=null] The default value if variable is not define)d
 * @returns {(object|string|array|number|boolean)} The env variable value).
 */
function envs (envvar: string, defaultValue: any = null) {
  const val = process.env[envvar];
  if (typeof val === 'undefined') {
    return defaultValue !== null ? defaultValue : null;
  }

  if (isNumber(val)) return parseFloat(val);
  if (TRUE_VALUES.indexOf(val) !== -1) return true;
  if (FALSE_VALUES.indexOf(val) !== -1) return false;

  return val;
}

/**
 * Return all env vars
 *
 * @param {boolean} [raw=false] will return the raw values from process.env.
 * @returns {object}
 */
envs.all = function all (raw = false) {
  if (raw) return process.env;
  return Object.keys(process.env).reduce((prev: any, key) => {
    prev[key] = envs(key);
    return prev;
  }, {});
};

/**
 * Sets an environment variable
 *
 * @param {string} envvar The variable name to be defined
 * @param {(object|string|array|number|boolean)} value The variable valu)e
 */
envs.set = function set (envvar: string, value: any) {
  process.env[envvar] = value;
};

/**
 * Unset an env variable
 *
 * @param {string} envvar The variable name to be unsetted
 */
envs.unset = function set (envvar: string) {
  if (typeof process.env[envvar] !== 'undefined') {
    delete process.env[envvar];
  }
};

/**
 * Find env vars matching names
 *
 * @param {string} name The name to be matched
 * @returns {(object|string|array|number|boolean)}
 */
envs.find = function find (name: string) {
  return Object.keys(process.env).filter((k) => k.indexOf(name) >= 0);
};

export default envs;
