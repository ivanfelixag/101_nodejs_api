import { Response } from "express";
import MessageService from "../services/Message.service";

export class MessageController {

  public async launchMessage(req: any, res: Response): Promise<void> {
    res.send(await MessageService.retriveMessage() + ' prueba');
  }

}
